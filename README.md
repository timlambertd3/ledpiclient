# ledpiclient

Repository for the various clients/client uis for interacting with the ledpi

This Repository  contains a UI and a small service behind it which will link into the ledpi's api. 

### UI Readme

The ui is the same for all 3 language branches, a basic react frontend, find more about it [here](./ledpi-client-ui/README.md)

### Branches

This repo is split into 3 branches representing 3 different languages, js node, java and python. The idea is that each branch represents a "master" branch for that particular language and extensions can be made off it during the workshops. 

Each branch is tagged with a "stable" base version which will be missing a few important elements and also will share the same ui code which will live in master and the 3 language branches will be kept up to date against that ui.

### Learning Goals

This project aims to show the students that you can interact with an api in many ways with many languages and will also demonstrate providing an api for the ui to use, each language branch will have unique parts missing or in need of editing so none of the problems across the branches are exactly the same, which is all part of the fun! There should be a big emphasis on enabling students to add interesting features, maybe someone thinks you could integrate some kind of logging into the system, or link a database into the system. If possible support and encourage this kind of creativity.
