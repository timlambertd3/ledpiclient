import React from "react";
import "./App.css";
import ImageList from "./components/ImageList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ImageList></ImageList>
      </header>
    </div>
  );
}

export default App;
