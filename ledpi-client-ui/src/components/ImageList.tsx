import Image from "../interfaces/image";
import React, { useEffect } from "react";
import { getImages, registerImage } from "../services/ImageService";
import {
  Container,
  Paper,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button
} from "@material-ui/core";
import LEDTableCell from "./shared/LEDTableCell";
import clsx from "clsx";
import Style from "./Style";

interface ImageProps {
  images?: Image[];
}

export default function ImageList(props: ImageProps) {
  const [images, setImages] = React.useState(props.images);
  const classes = Style();
  const largeHeightPaper = clsx(classes.paper, classes.paperFixedHeight);

  /**
   * How often the UI should auto-update the images list view (In milliseconds).
   */
  const updateTimer = 600000;

  /**
   * Sets the date the UI last got an image list from the images endpoint.
   */
  const [lastUpdated, setLastUpdated] = React.useState();

  /**
   * Checks whether there has ever been an update, or whether the minimum amount of time has accrued
   * since the previous update to warrant another.
   */
  const shouldUpdateImages = () => {
    // When undefined that means it's never been updated. So always get new data.
    if (!lastUpdated) {
      return true;
    } else {
      // Update every minute.
      return new Date().getTime() - lastUpdated.getTime() >= updateTimer;
    }
  };

  /**
   * Refreshes the images by sending off a request for new ones.
   */
  const refreshImages = () => {
    getImages().then(result => {
      if (result !== null) {
        setImages(result);
        setLastUpdated(new Date());
      }
    });
  };

  /**
   * Triggered when page is loaded, changed, or updated. Starts try get devices process.
   */
  useEffect(() => {
    if (shouldUpdateImages()) {
      refreshImages();
    }
  });

  return (
    <React.Fragment>
      <Container maxWidth="xl" className={classes.container}>
        <Paper className={largeHeightPaper}>
          <Typography color="inherit">Images</Typography>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Description</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {images ? (
                images.map(image => (
                  <TableRow key={image.name}>
                    <LEDTableCell>{image.name}</LEDTableCell>
                    <LEDTableCell>{image.description}</LEDTableCell>
                    <Button
                      color="primary"
                      variant="contained"
                      onClick={() => {
                        registerImage(image);
                      }}
                    >
                      Display
                    </Button>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <LEDTableCell>Loading...</LEDTableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Paper>
      </Container>
    </React.Fragment>
  );
}
