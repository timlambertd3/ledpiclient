import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";

const LEDTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white
    },
    body: {
      fontSize: 12,
      padding: 8
    }
  })
)(TableCell);

export const LEDTableCellLarge = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white
    },
    body: {
      fontSize: 12,
      padding: 15
    }
  })
)(TableCell);

export default LEDTableCell;
