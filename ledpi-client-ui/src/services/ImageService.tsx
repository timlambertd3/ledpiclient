import { ConfigValues } from "../utilities/ConfigValues";
import Image from "../interfaces/image";

/**
 *  Gets all images from the ledpi through a student microservice.
 */
export const getImages = () => {
  try {
    return new Promise<Image[]>(resolve => {
      fetch(ConfigValues.URL + "images", {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
        .then(response => response.json())
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          resolve();
        });
    });
  } catch (error) {
    return new Promise<Image[]>(resolve => {
      resolve();
    });
  }
};

/**
 * Registers an image with the ledpi routing through a student microservice.
 * @param image image to register.
 */
export const registerImage = (image: Image) => {
  try {
    fetch(ConfigValues.URL + "images", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(image)
    });
  } catch (error) {
    alert("error sending message!");
  }
};
