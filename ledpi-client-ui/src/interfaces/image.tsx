/**
 * Represents an image on the ledpi board. Description is optional as you don't need it when sending a post in, just the name.
 */
export default interface Image {
    name: string;
    description?: string;
}