# LEDPI UI

This simple react app provides a nice and simple ui for getting all the images and then being able to register them one after another, however this ui is not supposed to link straight into the ledpi, it needs to go through a service implemented in one of the 3 language branches, which are set up on localhost:8401. It expects a single /images endpoint that takes:

```
GET /images
    Headers:
        "Content-Type": "application/json"
    Body:
        None
    Returns:
        A list of images eg:
        [
            {
                "name": "deep3",
                "description": "We're deep3, a secure by design software company!"
            }
        ]

POST /images
    Headers:
        "Content-Type": "application/json"
        (A password is reaqired here for the call to the ledpi but should be populated in the service you're working on)
    Body:
        {
            "name": "deep3"
        }
    Returns:
        None, it's fire and forget, errors should be logged and debugged at the service underneath the ui
```